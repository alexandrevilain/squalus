package squalus

import (
	"database/sql"
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

type driver interface {
	adaptQuery(query string, params map[string]interface{}) (string, []interface{}, error)
}

func paramsRegexp() *regexp.Regexp {
	return regexp.MustCompile("{([a-zA-Z0-9_]+)}")
}

type paramsInfos struct {
	value  interface{}
	length int
}

func getParamsInfos(params map[string]interface{}) map[string]paramsInfos {
	infos := make(map[string]paramsInfos)
	for name, value := range params {
		i := paramsInfos{value: value, length: 1}
		t := reflect.TypeOf(value)
		if t.Kind() == reflect.Slice && t.Elem().Kind() != reflect.Uint8 {
			i.length = reflect.ValueOf(value).Len()
			if i.length == 1 {
				// replace value with the value of the only element in the slice
				i.value = reflect.ValueOf(value).Index(0).Interface()
			}
		}
		infos["{"+name+"}"] = i
	}
	return infos
}

// DB represents a Squalus connection.
type DB interface {
	driverName() string
	sqldb() *sql.DB
	Close()
	Exec(query string, params map[string]interface{}) (sql.Result, error)
	Query(query string, params map[string]interface{}, to interface{}) error
}

// Connection represents a database connection.
type Connection struct {
	db              *sql.DB
	driver          driver
	driverNameField string
}

func (cnx *Connection) driverName() string {
	return cnx.driverNameField
}

func (cnx *Connection) sqldb() *sql.DB {
	return cnx.db
}

// NewDB returns a new DB.
func NewDB(db *sql.DB) (DB, error) {
	// detect underlying database type and select corresponding driver
	var driver driver
	dbType := reflect.TypeOf(db.Driver()).String()
	driverName := ""
	switch {
	case strings.Contains(dbType, "MySQL"):
		driver = mysqlDriver{}
		driverName = "MySQL"
	case strings.Contains(dbType, "SQLite"):
		driver = mysqlDriver{} // SQLite is compatible with MySQL as far as Squalus is concerned
		driverName = "SQLite"
	case strings.Contains(dbType, "pq"):
		driver = postgresqlDriver{}
		driverName = "PostgreSQL"
	default:
		return nil, fmt.Errorf("Unsupported database driver: %s", dbType)
	}
	res := Connection{db: db, driver: driver, driverNameField: driverName}
	return &res, nil
}

// Close closes the underlying connection.
func (cnx *Connection) Close() {
	cnx.db.Close()
}

// getAllFieldsRec is a helper for getAllFields, defined because Go has some limitations on recursive functions
func getAllFieldsRec(val reflect.Value, fields *map[string]interface{}, prefix string) {
	typ := val.Type()
	for i := 0; i < typ.NumField(); i++ {
		field := val.Field(i)
		fieldType := typ.Field(i)
		if field.Kind() == reflect.Struct && !scanDirect(field.Type()) {
			p := ""
			tag, ok := fieldType.Tag.Lookup("db")
			if ok {
				p = tag
			} else {
				p = fieldType.Name
			}
			p += "."
			getAllFieldsRec(field, fields, prefix+p)
			if fieldType.Anonymous {
				// an "anonymous" struct results in two names for the same thing
				getAllFieldsRec(field, fields, prefix)
			}
		} else {
			key := fieldType.Name
			tag, ok := fieldType.Tag.Lookup("db")
			if ok {
				key = tag
			}
			(*fields)[prefix+key] = field.Addr().Interface()
		}
	}
}

// getAllFields returns all the fields of a struct, including those in embedded anonymous structs
func getAllFields(val reflect.Value) map[string]interface{} {
	fields := map[string]interface{}{} // field name => pointer to field
	getAllFieldsRec(val, &fields, "")
	return fields
}

// destsForStruct returns the destinations, suitable for Scan, for a given struct value.
func (cnx *Connection) destsForStruct(toVal reflect.Value, columns []string) ([]interface{}, error) {
	fields := getAllFields(toVal)

	dests := []interface{}{}
	for _, column := range columns {
		f, ok := fields[column]
		if !ok {
			return nil, fmt.Errorf("Column %v has no corresponding field in struct %v", column, toVal.Type().Name())
		}
		dests = append(dests, f)
	}
	return dests, nil
}

// selectAllRows reads all rows for the query and arguments, calling the function for each row.
func (cnx *Connection) selectAllRows(query string, args []interface{}, cb func(rows *sql.Rows) error) error {
	rows, err := cnx.db.Query(query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		if err := cb(rows); err != nil {
			return err
		}
	}
	return rows.Err()
}

func scanDirect(typ reflect.Type) bool {
	_, b := reflect.PtrTo(typ).MethodByName("Scan")
	if !b {
		b = typ.String() == "time.Time"
	}
	return b
}

// scanRowIntoNewValue scans the values in a row into a new instance of the given type and returns it.
func (cnx *Connection) scanRowIntoNewValue(rows *sql.Rows, rType reflect.Type) (*reflect.Value, error) {
	switch rType.Kind() {
	case reflect.Slice:
		if rType.Elem().Kind() != reflect.Uint8 {
			return nil, fmt.Errorf("Unsupported type for scanning SQL query results: %v", rType)
		}
		fallthrough // byte slices are treated like basic types
	case reflect.Bool, reflect.String,
		reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Float32, reflect.Float64:
		val := reflect.New(rType)
		if err := rows.Scan(val.Interface()); err != nil {
			return nil, err
		}
		e := val.Elem()
		return &e, nil
	case reflect.Struct:
		scanDirect := scanDirect(rType)
		val := reflect.New(rType)
		columns, err := rows.Columns()
		if err != nil {
			return nil, err
		}
		dests := []interface{}{val.Interface()}
		if !scanDirect {
			var err error
			dests, err = cnx.destsForStruct(reflect.Indirect(val), columns)
			if err != nil {
				return nil, err
			}
		}
		if err := rows.Scan(dests...); err != nil {
			return nil, err
		}
		e := val.Elem()
		return &e, nil
	}
	return nil, fmt.Errorf("Unsupported type for scanning SQL query results: %v", rType)
}

// Query runs a query with parameters and returns the results.
func (cnx *Connection) Query(query string, params map[string]interface{}, to interface{}) error {
	adQuery, adArgs, err := cnx.driver.adaptQuery(query, params)
	if err != nil {
		return err
	}
	var values []reflect.Value
	var scanValues []interface{}
	toVal := reflect.ValueOf(to)
	toType := toVal.Type()
	switch toType.Kind() {
	case reflect.Ptr:
		switch toType.Elem().Kind() {
		case reflect.Struct:
			if scanDirect(toType.Elem()) {
				return cnx.db.QueryRow(adQuery, adArgs...).Scan(to)
			}
			rows, err := cnx.db.Query(adQuery, adArgs...)
			if err != nil {
				return err
			}
			defer rows.Close()
			if !rows.Next() {
				return sql.ErrNoRows
			}
			columns, err := rows.Columns()
			if err != nil {
				return err
			}
			dests, err := cnx.destsForStruct(toVal.Elem(), columns)
			if err != nil {
				return err
			}
			if err := rows.Scan(dests...); err != nil {
				return err
			}
			return rows.Err()
		case reflect.Slice:
			if toType.Elem().Elem().Kind() != reflect.Uint8 {
				slice := reflect.New(toType.Elem()).Elem()
				if err := cnx.selectAllRows(adQuery, adArgs, func(rows *sql.Rows) error {
					value, err := cnx.scanRowIntoNewValue(rows, slice.Type().Elem())
					if err != nil {
						return err
					}
					slice = reflect.Append(slice, *value)
					return nil
				}); err != nil {
					return err
				}
				toVal.Elem().Set(slice)
				return nil
			}
			fallthrough // []byte is treated like a basic type
		case reflect.Bool, reflect.String,
			reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
			reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
			reflect.Float32, reflect.Float64:
			return cnx.db.QueryRow(adQuery, adArgs...).Scan(to)
		}
	case reflect.Chan:
		defer toVal.Close()
		return cnx.selectAllRows(adQuery, adArgs, func(rows *sql.Rows) error {
			value, err := cnx.scanRowIntoNewValue(rows, toType.Elem())
			if err != nil {
				return err
			}
			toVal.Send(*value)
			return nil
		})
	case reflect.Func:
		for i := 0; i < toType.NumIn(); i++ {
			v := reflect.New(toType.In(i))
			values = append(values, v)
			scanValues = append(scanValues, v.Interface())
		}
		return cnx.selectAllRows(adQuery, adArgs, func(rows *sql.Rows) error {
			err := rows.Scan(scanValues...)
			if err != nil {
				return err
			}
			var cbValues []reflect.Value
			for _, v := range values {
				cbValues = append(cbValues, reflect.Indirect(v))
			}
			toVal.Call(cbValues)
			return nil
		})
	}
	return fmt.Errorf("squalus.Query: type of to (%s) is not supported", reflect.TypeOf(to))
}

type result struct {
	lastInsertIDValue int64
	lastInsertIDError error
	rowsAffectedValue int64
	rowsAffectedError error
}

func (r result) LastInsertId() (int64, error) {
	return r.lastInsertIDValue, r.lastInsertIDError
}

func (r result) RowsAffected() (int64, error) {
	return r.rowsAffectedValue, r.rowsAffectedError
}

// Exec executes a query without returning any rows.
func (cnx *Connection) Exec(query string, params map[string]interface{}) (sql.Result, error) {
	adQuery, adParams, err := cnx.driver.adaptQuery(query, params)
	if err != nil {
		return result{}, err
	}
	return cnx.db.Exec(adQuery, adParams...)
}
