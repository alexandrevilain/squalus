# Squalus — SQL made pleasant

Squalus is a small package that makes it much easier to perform SQL queries in Go by encapsulating cursor manipulation, error handling and results fetching into a simple API. It exposes an interface that hides some of the most annoying differences between SQL drivers and allows to use named parameters even if the underlying engine does not support them.

## squalus.DB creation

Create a `sql.DB` as usual, then a `squalus.DB` from it.

```go
db1, err := sql.Open("driver name", "some connection string")
if err != nil {
	// handle err
}
db, err := squalus.NewDB(db1)
if err != nil {
	// handle err
}
defer db.Close()
```

Squalus automatically detects the driver type. Supported drivers are:
* [Mysql](https://github.com/go-sql-driver/mysql) (go-sql-driver/mysql)
* [PostgreSQL](https://github.com/lib/pq) (lib/pq)
* [SQLite3](https://github.com/mattn/go-sqlite3) (mattn/sqlite3)

Attempting to create a DB with another driver type results in an error.

## Examples setting

The following examples use a table in which data about persons are stored. Here is the corresponding struct:

```go
type Person struct {
	ID        int       `db:"id"`     // notice the db tag
	Name      string    `db:"name"`
	Height    float64   `db:"height"` // in meters
	BirthDate time.Time `db:"birth"`
}
```

## Query execution

Just like `sql.DB`, `squalus.DB` provides an `Exec` method.

```go
db.Exec("create table [persons]([id] int, [name] varchar(128), [height] float, [birth] datetime)", nil)
result, err = db.Exec(
	"insert into [persons]([id], [name], [height], [birth]) values({id}, {name}, {height}, {birth})",
	map[string]interface{}{
		"id":    1,
		"name": "Alice Abbott",
        "height": 1.65,
        "birth": time.Date(1985, 7, 12, 0, 0, 0, 0, time.UTC)
	},
)
if err != nil {
	// handle err
}
// result is the regular sql.Result
```

This example shows that Squalus uses square brackets as database, table and field delimiters. They are automatically replaced by whatever the underlying driver requires, and of course, they can be omitted when not needed. MySQL users will appreciate finally being able to use backticks for long queries in their Go code.

It also shows how query parameters work. Only named parameters are supported, and they are passed through a `map[string]interface{}`, which can be `nil` if no parameters are provided.

## Data acquisition

`Query` is the only method that Squalus provides to read data. Its behaviour depends on the type of the `to` parameter.

The following examples assume that the table contains the rows below:

| ID  | Name            | Height  | Birth      |
| --- | --------------- | ------- | ---------- |
| 1   | Alice Abbott    | 1.65    | 1985-07-11 |
| 2   | Bob Burton      | 1.59    | 1977-03-01 |
| 3   | Clarissa Cooper | 1.68    | 2003-09-30 |
| 4   | Donald Dock     | 1.71    | 1954-12-04 |

### Query to a single value

To read a single value, use a pointer to a basic type as the value of `to`.

```go
var name string
if err := db.Query(
	"select [name] from [persons] where [id]={id}",
	map[string]interface{}{"id": 3},
	&name,
); err != nil {
	// handle err
}
// name contains "Clarissa Cooper"
```

If no rows are found, Query returns `sql.ErrNoRows`.

As a special case, `time.Time` is treated like a basic type, so it behaves as expected.

```go
var birthDate time.Time
if err := db.Query(
	"select [birth] from [persons] where [id]={id}",
	map[string]interface{}{"id": 3},
	&birthDate,
); err != nil {
	// handle err
}
// birthDate == time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC)
```

### Query to a struct

You can read one multicolumn row directly into a struct.

```go
var person Person
if err := db.Query(
	"select [name], [id], [birth], [height] from [persons] where [id]={id}",
	map[string]interface{}{"id": 3},
	&person,
); err != nil {
	// handle err
}
// person contains the data for Clarissa Cooper
```

Struct composition is supported, with the same rules for naming fields as in Go. The one exception is that if a ```db``` tag is given, it replaces the field name.
This makes it easier to work with joins and other scenarios in which several fields bear the same name.
For example, the example above also works with the following definition of Person, because the structs are embedded (anonymous):

```go
type Height struct {
	Height float64 `db:"height"`
}
type NameBirthHeight struct {
	Name      string    `db:"name"`
	BirthDate time.Time `db:"birth"`
	Height
}
type Person struct {
	ID int `db:"id"`
	NameBirthHeight
}
```

This example illustrates the handling of named structs:

```go
type Height struct {
	Height float64 `db:"height"`
}
type NameBirthHeight struct {
	Name      string    `db:"name"`
	BirthDate time.Time `db:"birth"`
	H         Height    `db:"hh"`
}
type PersonComposed struct {
	ID  int `db:"id"`
	NBH NameBirthHeight
}

var person1 PersonComposed
if err := db.Query(
	"select [name] as [NBH.name], [id], [birth] as [NBH.birth], [height] as [NBH.hh.height] from [persons] where [id]={id}",
	map[string]interface{}{"id": 3},
	&person1,
); err != nil {
	// handle err
}
```

### Query to a slice

If `to` is a pointer to a slice, Squalus fills the slice with all the data returned by the query. The rules for handling basic types and structs are applied to the slice type.

```go
var people []Person
if err := db.Query(
	"select [name], [id], [birth], [height] from [persons] order by [id]",
	nil,
	&people,
); err != nil {
	// handle err
}
// people contains all four persons
```

Notice how there is still exactly one place where an error may be returned, even though several rows were read from database.

### Query to a channel

If `to` is a channel, every row will be read and sent to that channel. Squalus closes the channel when there are no more data.

```go
ch := make(chan Person)

go func() {
	for p := range ch {
		fmt.Println(p)
	}
}()

if err := db.Query(
	"select [name], [id], [birth], [height] from [persons] order by [id]",
	nil,
	ch,
); err != nil {
	// handle err
}
// all people are printed to stdout
```

### Query using a callback

If `to` is a function, it is called once for each row. Columns and callback parameters are matched by rank only, not by name: each column, in the order of the `select` clause, matches the corresponding function parameter. Struct parameters are scanned directly, without applying the mechanism described above to match columns to struct fields.

```go
if err := db.Query(
	"select [name], [id], [birth], [height] from [persons] order by [id]",
	nil,
	func(name string, id int, birthDate time.Time, height float64) {
		fmt.Printf("%v has ID %v, birth date %v and height %v\n", name, id, birthDate, height)
	},
); err != nil {
	// handle err
}
// all people are printed to stdout
```

### Structs that have a Scan method

If a struct has a `Scan` method with a pointer receiver, it is treated like a basic type, so it behaves as expected.

```go
type NameResult struct {
	First string
	Last  string
}

func (nr *NameResult) Scan(src interface{}) error {
	// some drivers return a string here, some return a []byte
	s, ok := src.(string)
	if !ok {
		b, ok := src.([]byte)
		if ok {
			s = string(b)
		} else {
			return fmt.Errorf("Scan: could not aquire field value as string or []byte")
		}
	}
	t := strings.Split(s, " ")
	if len(t) != 2 {
		return fmt.Errorf("format of %v is wrong", s)
	}
	nr.First, nr.Last = t[0], t[1]
	return nil
}

func getNames() {
    var names []NameResult
	if err := db.Query(
		"select [name] from [persons] order by [id]",
		nil,
		&names,
	); err != nil {
		// handle err
    }
    // names contains the names of everybody
}
```

### Writing IN clauses

Squalus makes it easy to perform a `select` with an `in` clause: if the value of a parameter is a slice, it is expanded automatically.

```go
var people []Person
if err := db.Query(
	"select [name], [id], [birth], [height] from [persons] where [id] in ({ids}) order by [id]",
	map[string]interface{}{"ids": []int{1, 3, 4}},
	&people,
); err != nil {
	// handle err
}
// people contains Alice Abbott, Clarissa Cooper and Donald Dock
```

This rule does not apply to byte slices (and uint8 slices, since Go does not distinguish internally between byte and uint8), in order to facilitate loading and storing data between []byte and blob.

## License

Squalus is released under the MIT license, as found in the LICENSE file and below.

Copyright (C) 2017 QOS Energy

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
