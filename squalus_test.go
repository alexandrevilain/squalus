package squalus

import (
	"database/sql"
	sqldriver "database/sql/driver"
	"fmt"
	"os"
	"reflect"
	"runtime"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type DummyTx struct{}

func (d DummyTx) Commit() error {
	return nil
}

func (d DummyTx) Rollback() error {
	return nil
}

type DummyConn struct{}

func (d DummyConn) Prepare(query string) (sqldriver.Stmt, error) {
	return nil, nil
}

func (d DummyConn) Close() error {
	return nil
}

func (d DummyConn) Begin() (sqldriver.Tx, error) {
	return DummyTx{}, nil
}

type DummyDriver struct{}

func (d DummyDriver) Open(name string) (sqldriver.Conn, error) {
	return DummyConn{}, nil
}

type Person struct {
	ID        int       `db:"id"`
	Name      string    `db:"name"`
	Height    float64   `db:"height"` // in meters
	BirthDate time.Time `db:"birth"`
}

var persons = []Person{
	{
		ID:        1,
		Name:      "Alice Abbott",
		Height:    1.65,
		BirthDate: time.Date(1985, 7, 11, 0, 0, 0, 0, time.UTC),
	},
	{
		ID:        2,
		Name:      "Bob Burton",
		Height:    1.59,
		BirthDate: time.Date(1977, 3, 1, 0, 0, 0, 0, time.UTC),
	},
	{
		ID:        3,
		Name:      "Clarissa Cooper",
		Height:    1.68,
		BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
	},
	{
		ID:        4,
		Name:      "Donald Dock",
		Height:    1.71,
		BirthDate: time.Date(1954, 12, 4, 0, 0, 0, 0, time.UTC),
	},
}

func TestSqualus(t *testing.T) {
	sql.Register("dummy", DummyDriver{})

	tests := []func(t *testing.T, db DB){
		PopulateDatabase,
		QueryString,
		QueryFail,
		QueryMissingParam,
		QueryTime,
		QueryStruct,
		QueryStructEmbedded,
		QueryStructComposed,
		QueryStructFail,
		QuerySliceOfString,
		QuerySliceOfStruct,
		QuerySliceOfStructWithInClause,
		QueryChanOfStruct,
		QueryCallback,
		QuerySliceOfStructsWithScan,
		ExecFail,
		NewDBFail,
		ByteSliceOperations,
		DeleteWithInClause,
	}

	mkTest := func(f func(t *testing.T, db DB), db DB) func(t *testing.T) {
		return func(t *testing.T) {
			f(t, db)
		}
	}

	dbs := []DB{
		ConnectToMysql(t),
		ConnectToSQLite(t),
		ConnectToPostgresql(t),
	}
	for _, db := range dbs {
		defer db.Close()
	}
	defer os.Remove("./test.db")

	for _, db := range dbs {
		for _, test := range tests {
			spl := strings.Split(runtime.FuncForPC(reflect.ValueOf(test).Pointer()).Name(), ".")
			funcName := spl[len(spl)-1]
			t.Run(db.driverName()+"-"+funcName, mkTest(test, db))
		}
	}
}

func PopulateDatabase(t *testing.T, db DB) {
	for _, p := range persons {
		result, err := db.Exec(
			"insert into [persons]([id], [name], [height], [birth]) values({id}, {name}, {height}, {birth})",
			map[string]interface{}{
				"id":     p.ID,
				"name":   p.Name,
				"height": p.Height,
				"birth":  p.BirthDate,
			},
		)
		if err != nil {
			t.Fatalf("Failed to Exec: %s", err)
		}
		rowsAffected, _ := result.RowsAffected()
		assert.Equal(t, int64(1), rowsAffected, "Rows affected")
	}
}

func QueryString(t *testing.T, db DB) {
	var name string
	if err := db.Query(
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&name,
	); err != nil {
		t.Fatalf("Failed to read person name: %s", err)
	}
	assert.Equal(t, "Clarissa Cooper", name, "Person name")
}

func QueryFail(t *testing.T, db DB) {
	var result string
	err := db.Query(
		"Be-Bop-A-Lula",
		map[string]interface{}{"id": 3},
		&result,
	)
	assert.Error(t, err, "Query (to string) should fail because of an invalid query")

	var results []string
	err = db.Query(
		"Be-Bop-A-Lula",
		map[string]interface{}{"id": 3},
		&results,
	)
	assert.Error(t, err, "Query (to slice) should fail because of an invalid query")

	var resultsInt []int
	err = db.Query(
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&resultsInt,
	)
	assert.Error(t, err, "Query (to slice) should fail because of an invalid result type")

	var resultsIntSlice [][]int
	err = db.Query(
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&resultsIntSlice,
	)
	assert.Error(t, err, "Query (to slice of slice of ints) should fail because of an invalid result type")

	resultsArray := [1]Person{}
	err = db.Query(
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&resultsArray,
	)
	assert.Error(t, err, "Query (to array) should fail because array is not a supported result type")

	var dummies [][1]string
	err = db.Query(
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&dummies,
	)
	assert.Error(t, err, "Query (to slice of arrays) should fail because a slice of arrays is not a supported result type")
}

func QueryMissingParam(t *testing.T, db DB) {
	var name string
	err := db.Query(
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"kangaroo": 42},
		&name,
	)
	assert.Error(t, err, "Query should fail because of a missing parameter")
}

func QueryTime(t *testing.T, db DB) {
	var birthDate time.Time
	if err := db.Query(
		"select [birth] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&birthDate,
	); err != nil {
		t.Fatalf("Failed to read person birth date: %s", err)
	}
	assert.Equal(t, time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC), birthDate, "Person birth date")
}

func QueryStruct(t *testing.T, db DB) {
	clarissaCooper := Person{
		ID:        3,
		Name:      "Clarissa Cooper",
		Height:    1.68,
		BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
	}
	var person Person
	if err := db.Query(
		"select [name], [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person, "Person data")
}

func QueryStructEmbedded(t *testing.T, db DB) {
	type Height struct {
		Height float64 `db:"height"`
	}
	type NameBirthHeight struct {
		Name      string    `db:"name"`
		BirthDate time.Time `db:"birth"`
		Height
	}
	type PersonEmbed struct {
		ID              int `db:"id"`
		NameBirthHeight `db:"name_birth_height"`
	}
	clarissaCooper := PersonEmbed{
		ID: 3,
		NameBirthHeight: NameBirthHeight{
			Name:      "Clarissa Cooper",
			BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
			Height: Height{
				Height: 1.68,
			},
		},
	}

	var person1 PersonEmbed
	if err := db.Query(
		"select [name], [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person1,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person1, "Person data")

	var person2 PersonEmbed
	if err := db.Query(
		"select [name] as [name_birth_height.name], [id], [birth], [height] as [name_birth_height.height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person2,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person2, "Person data")

	var person3 PersonEmbed
	if err := db.Query(
		"select [name] as [name_birth_height.name], [id], [birth], [height] as [name_birth_height.Height.height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person3,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person3, "Person data")
}

func QueryStructComposed(t *testing.T, db DB) {
	type Height struct {
		Height float64 `db:"height"`
	}
	type NameBirthHeight struct {
		Name      string    `db:"name"`
		BirthDate time.Time `db:"birth"`
		H         Height    `db:"hh"`
	}
	type PersonComposed struct {
		ID  int `db:"id"`
		NBH NameBirthHeight
	}
	clarissaCooper := PersonComposed{
		ID: 3,
		NBH: NameBirthHeight{
			Name:      "Clarissa Cooper",
			BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
			H: Height{
				Height: 1.68,
			},
		},
	}

	var person1 PersonComposed
	if err := db.Query(
		"select [name] as [NBH.name], [id], [birth] as [NBH.birth], [height] as [NBH.hh.height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person1,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person1, "Person data")
}

func QueryStructFail(t *testing.T, db DB) {
	var person Person

	err := db.Query(
		"select [name], [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 42},
		&person,
	)
	if err == nil {
		t.Fatal("Query should fail to read a nonexistent person")
	}
	assert.Equal(t, err, sql.ErrNoRows)

	err = db.Query(
		"select [name], [id], [birth], [height], 42 as [forty_two] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person,
	)
	assert.Error(t, err, "Query to struct should fail because of a missing field")

	err = db.Query(
		"select [name], 'Beep' as [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person,
	)
	assert.Error(t, err, "Query to struct should fail because of a field type mismatch")

	var people []Person
	err = db.Query(
		"select [name], [id], [birth], [height], 42 as [forty_two] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&people,
	)
	assert.Error(t, err, "Query to []struct should fail because of a missing field")

	peopleChan := make(chan Person)
	err = db.Query(
		"select [name], [id], [birth], [height], 42 as [forty_two] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		peopleChan,
	)
	assert.Error(t, err, "Query to chan struct should fail because of a missing field")

	err = db.Query(
		"select [name], [id], [birth], [height], 42 as [forty_two] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		func() {},
	)
	assert.Error(t, err, "Query to struct via callback should fail because of a missing field")

	err = db.Query(
		"select [name], 'Beep' as [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&people,
	)
	assert.Error(t, err, "Query to []struct should fail because of a field type mismatch")

	err = db.Query(
		"I see a red door",
		map[string]interface{}{"id": 3},
		&person,
	)
	assert.Error(t, err, "Query to struct should fail because of an invalid query")
}

func QuerySliceOfString(t *testing.T, db DB) {
	var names []string
	if err := db.Query(
		"select [name] from [persons] order by [id]",
		nil,
		&names,
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	assert.Equal(t, []string{"Alice Abbott", "Bob Burton", "Clarissa Cooper", "Donald Dock"}, names, "Persons")
}

func QuerySliceOfStruct(t *testing.T, db DB) {
	var people []Person
	if err := db.Query(
		"select [name], [id], [birth], [height] from [persons] order by [id]",
		nil,
		&people,
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	assert.Equal(t, persons, people, "Persons")
}

func QuerySliceOfStructWithInClause(t *testing.T, db DB) {
	expected := []Person{
		{
			ID:        1,
			Name:      "Alice Abbott",
			Height:    1.65,
			BirthDate: time.Date(1985, 7, 11, 0, 0, 0, 0, time.UTC),
		},
		{
			ID:        3,
			Name:      "Clarissa Cooper",
			Height:    1.68,
			BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
		},
		{
			ID:        4,
			Name:      "Donald Dock",
			Height:    1.71,
			BirthDate: time.Date(1954, 12, 4, 0, 0, 0, 0, time.UTC),
		},
	}

	var people []Person
	if err := db.Query(
		"select [name], [id], [birth], [height] from [persons] where [id] in ({ids}) order by [id]",
		map[string]interface{}{"ids": []int{1, 3, 4}},
		&people,
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	assert.Equal(t, expected, people, "Persons")
}

func QueryChanOfStruct(t *testing.T, db DB) {
	ch := make(chan Person)
	var people []Person
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		for p := range ch {
			people = append(people, p)
		}
		wg.Done()
	}()
	if err := db.Query(
		"select [name], [id], [birth], [height] from [persons] order by [id]",
		nil,
		ch,
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	wg.Wait()
	assert.Equal(t, persons, people, "Persons")
}

func QueryCallback(t *testing.T, db DB) {
	var people []Person
	if err := db.Query(
		"select [name], [id], [birth], [height] from [persons] order by [id]",
		nil,
		func(name string, id int, birthDate time.Time, height float64) {
			people = append(people, Person{ID: id, Name: name, BirthDate: birthDate, Height: height})
		},
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	assert.Equal(t, persons, people, "Persons")
}

type NameResult struct {
	First string
	Last  string
}

func (nr *NameResult) Scan(src interface{}) error {
	// some drivers return a string here, some return a []byte
	s, ok := src.(string)
	if !ok {
		b, ok := src.([]byte)
		if ok {
			s = string(b)
		} else {
			return fmt.Errorf("Scan: could not acquire field value as string or []byte")
		}
	}
	t := strings.Split(s, " ")
	if len(t) != 2 {
		return fmt.Errorf("format of %v is wrong", s)
	}
	nr.First, nr.Last = t[0], t[1]
	return nil
}

func QuerySliceOfStructsWithScan(t *testing.T, db DB) {
	expected := []NameResult{
		{
			First: "Alice",
			Last:  "Abbott",
		},
		{
			First: "Bob",
			Last:  "Burton",
		},
		{
			First: "Clarissa",
			Last:  "Cooper",
		},
		{
			First: "Donald",
			Last:  "Dock",
		},
	}
	var names []NameResult
	if err := db.Query(
		"select [name] from [persons] order by [id]",
		nil,
		&names,
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	assert.Equal(t, expected, names, "Names")
}

func ExecFail(t *testing.T, db DB) {
	result, err := db.Exec("insert into [persons]([id]) values({id})", nil)
	assert.Error(t, err, "Exec should fail because of a missing parameter")
	i, err := result.LastInsertId()
	assert.Zero(t, i, "Last inserted ID should be zero")
	assert.NoError(t, err, "Last inserted ID should return no error")
	r, err := result.RowsAffected()
	assert.Zero(t, r, "Rows affected should be zero")
	assert.NoError(t, err, "Last inserted ID should return no error")
}

func NewDBFail(t *testing.T, db DB) {
	db1, err := sql.Open("dummy", "")
	if err != nil {
		t.Fatalf("Error: %s", err)
	}
	_, err = NewDB(db1)
	assert.Error(t, err, "Creating a Squalus DB should fail because the dummy driver is not supported")
}

func ByteSliceOperations(t *testing.T, db DB) {
	val := []byte{1, 2, 3, 4}
	params := map[string]interface{}{"val": val}
	result, err := db.Exec("insert into [testbinary]([data]) values({val})", params)
	assert.NoError(t, err)
	rowsAffected, err := result.RowsAffected()
	assert.NoError(t, err)
	assert.Equal(t, int64(1), rowsAffected, "1 row should have been inserted")

	val2 := []byte{5, 6, 7, 8}
	params2 := map[string]interface{}{"val": val2}
	result, err = db.Exec("insert into [testbinary]([data]) values({val})", params2)
	assert.NoError(t, err)
	rowsAffected, err = result.RowsAffected()
	assert.NoError(t, err)
	assert.Equal(t, int64(1), rowsAffected, "1 row should have been inserted")

	count := 0
	err = db.Query("select count(*) from [testbinary] where [data]={val}", params, &count)
	assert.NoError(t, err)
	assert.Equal(t, count, 1, "1 row should have been found")

	var b []byte
	err = db.Query("select [data] from [testbinary] where [data]={val}", params, &b)
	assert.NoError(t, err)
	assert.Equal(t, b, val, "Read value should be the same as written value")

	var bs [][]byte
	err = db.Query(
		"select [data] from [testbinary] where [data] in ({val}) order by [data]",
		map[string]interface{}{"val": [][]byte{val, val2}},
		&bs,
	)
	assert.NoError(t, err)
	assert.Equal(t, 2, len(bs), "2 rows should have been read")
	assert.Equal(t, bs[0], val, "Read value should be the same as written value")
	assert.Equal(t, bs[1], val2, "Read value should be the same as written value")
}

func DeleteWithInClause(t *testing.T, db DB) {
	{
		// test empty IN clause
		_, errSQL := db.sqldb().Exec("delete from persons where id in ()")
		_, errSqualus := db.Exec(
			"delete from persons where id in ({ids})",
			map[string]interface{}{"ids": []int64{}},
		)
		assert.Equal(t, errSQL, errSqualus, "Empty IN clause should not cause a Squalus error")
	}

	{
		// test IN clause containing exactly one element (particular case in Squalus)
		result, err := db.Exec(
			"delete from [persons] where [id] in ({ids})",
			map[string]interface{}{"ids": []int64{1}},
		)
		assert.NoError(t, err)
		ra, err := result.RowsAffected()
		assert.NoError(t, err, "Getting rows affected should succeed")
		assert.Equal(t, int64(1), ra, "There should be one row affected")
	}

	{
		// test IN clause containing several elements
		result, err := db.Exec(
			"delete from [persons] where [id] in ({ids})",
			map[string]interface{}{"ids": []int64{2, 3, 4}},
		)
		assert.NoError(t, err)
		ra, err := result.RowsAffected()
		assert.NoError(t, err, "Getting rows affected should succeed")
		assert.Equal(t, int64(3), ra, "There should be 3 rows affected")
	}
}
