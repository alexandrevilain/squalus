package squalus

import (
	"database/sql"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
)

func ConnectToMysql(t *testing.T) DB {
	db, err := sql.Open("mysql", "u:p@tcp(127.0.0.1:13306)/db?parseTime=true")
	errPing := db.Ping()
	if err != nil || errPing != nil {
		db, err = sql.Open("mysql", "u:p@tcp(mysql:3306)/db?parseTime=true")
		if err != nil {
			t.Fatalf("Failed to connect to Mysql server: %s", err)
		}
	}
	err = db.Ping()
	if err != nil {
		t.Fatalf("Failed to connect to Mysql server: %s", err)
	}
	sdb, err := NewDB(db)
	if err != nil {
		t.Fatalf("Failed to create Squalus DB: %s", err)
	}

	sdb.Exec("drop table if exists [persons]", nil)
	_, err = sdb.Exec("create table [persons]([id] int, [name] varchar(128), [height] float, [birth] datetime)", nil)
	assert.NoError(t, err, "Table creation should succeed")

	sdb.Exec("drop table if exists [testbinary]", nil)
	_, err = sdb.Exec("create table [testbinary]([data] blob)", nil)
	assert.NoError(t, err, "Table creation should succeed")

	return sdb
}
